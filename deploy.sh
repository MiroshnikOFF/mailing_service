#python3 -m venv env
#source env/bin/activate
#pip3 install -r requirements.txt
#python3 manage.py migrate
#python3 manage.py collectstatic
#deactivate

docker-compose down
docker-compose up --build -d
docker-compose exec app python manage.py create_groups
docker-compose exec app python manage.py ccsu
